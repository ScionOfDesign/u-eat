﻿/****************************************************************************/
/*!
\file   ActionBase.cs
\author Joshua Biggs
\par    Email: Joshua.B@Digipen.edu
\par    Developed: Summer 2016
\brief

Contains the base ActionBase class that all derivitive action types inherit from.
All actions can be Paused, Resumed, Completed, and Restarted.

© 2016 Joshua Biggs CC Attribution
*/
/****************************************************************************/
using UnityEngine;

namespace ActionSystem
{
    namespace Internal
    {
        /// <summary>
        ///   Provides interpolation functions for Vec3, Color, and float.
        /// </summary>
        public class ActionBase
        {
            //Booleans for different states of the current action.
            protected bool Paused = false;
            protected bool Completed = false;

            public void Pause() { Paused = true; }
            public void Resume() { Paused = false; }
            public bool IsPaused() { return Paused; }
            public bool IsCompleted() { return Completed; }
            
            public virtual void InstantComplete()
            {
                while(!Completed)
                {
                    Update(int.MaxValue);
                }
            }

            //I have Update as a virtual function in order to make adding new action types incredibly simple.
            public virtual void Update(double dt) { }
            //Restart is virtual in the case that the user wants to ake their own custom 
            //derived Action class with different restart functionality.
            public virtual void Restart() { Completed = false; }

            //Protected so that this class is always a base class.
            protected ActionBase() { }

            #region DefaultInterpolationFunctions

            static ActionBase()
            {
                GenericCalculator<Color, Color, Color>.AddFunc = ColorAdd;
                GenericCalculator<Color, Color, Color>.SubtractFunc = ColorSub;
                GenericCalculator<Color, float, Color>.MultiplyFunc = ColorMult;
                GenericCalculator<Color, float, Color>.DivideFunc = ColorDiv;

                GenericCalculator<Vector3, Vector3, Vector3>.AddFunc = Vec3Add;
                GenericCalculator<Vector3, Vector3, Vector3>.SubtractFunc = Vec3Sub;
                GenericCalculator<Vector3, float, Vector3>.MultiplyFunc = Vec3Mult;
                GenericCalculator<Vector3, float, Vector3>.DivideFunc = Vec3Div;

                GenericCalculator<float, float, float>.AddFunc = FloatAdd;
                GenericCalculator<float, float, float>.SubtractFunc = FloatSub;
                GenericCalculator<float, float, float>.MultiplyFunc = FloatMult;
                GenericCalculator<float, float, float>.DivideFunc = FloatDiv;
            }

            static Color ColorAdd(Color lhs, Color rhs)
            {
                return lhs + rhs;
            }

            static Color ColorSub(Color lhs, Color rhs)
            {
                return lhs - rhs;
            }

            static Color ColorMult(Color lhs, float rhs)
            {
                return lhs * rhs;
            }

            static Color ColorDiv(Color lhs, float rhs)
            {
                return lhs / rhs;
            }

            static Vector3 Vec3Add(Vector3 lhs, Vector3 rhs)
            {
                return lhs + rhs;
            }

            static Vector3 Vec3Sub(Vector3 lhs, Vector3 rhs)
            {
                return lhs - rhs;
            }

            static Vector3 Vec3Mult(Vector3 lhs, float rhs)
            {
                return lhs * rhs;
            }

            static Vector3 Vec3Div(Vector3 lhs, float rhs)
            {
                return lhs / rhs;
            }

            static float FloatAdd(float lhs, float rhs)
            {
                return lhs + rhs;
            }

            static float FloatSub(float lhs, float rhs)
            {
                return lhs - rhs;
            }

            static float FloatMult(float lhs, float rhs)
            {
                return lhs * rhs;
            }

            static float FloatDiv(float lhs, float rhs)
            {
                return lhs / rhs;
            }
            #endregion
        };
    }//namespace Internal
} //namespace ActionSystem
