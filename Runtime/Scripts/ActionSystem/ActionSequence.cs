﻿/****************************************************************************/
/*!
\file   ActionSequence.cs
\author Joshua Biggs
\par    Email: Joshua.B@Digipen.edu
\par    Developed: Summer 2016
\brief

The ActionSequence contains a list of other actions (Properties, Groups, Calls,
Sequences, etc.) that are put into a queue and run one after the other.

© 2016 Joshua Biggs CC Attribution
*/
/****************************************************************************/
using System.Collections.Generic;
using System.Linq;
using ActionSystem.Internal;

namespace ActionSystem
{
    /// <summary>
    ///   Action which holds, and steps through, a sequence of other actions.
    ///   Can loop.
    /// </summary>
    public class ActionSequence : ActionBase
    {
        public bool LoopingSequence { get; set; }
        private List<ActionBase> ActionQueue;
        private ActionBase CurrentAction;
        public int Index { get; set; }

        public ActionSequence(bool looping = false) : base()
        {
            LoopingSequence = looping;
            ActionQueue = new List<ActionBase>();
        }

        public ActionSequence(List<ActionBase> actionQueue, bool looping = false) : base()
        {
            LoopingSequence = looping;
            ActionQueue = actionQueue;
            CurrentAction = ActionQueue.First();
        }

        /// <summary>
        ///   Step position in action queue.
        ///   Calls update on current Action.
        /// </summary>
        public override void Update(double dt)
        {
            
            if (IsPaused() || IsCompleted())
            {
                return;
            }
            if (IsEmpty())
            {
                Completed = true;
                return;
            }

            if (CurrentAction.IsCompleted())
            {
                if (!LoopingSequence)
                {

                    ActionQueue.RemoveAt(0);
                    if (ActionQueue.Count == 0)
                    {
                        Completed = true;
                        return;
                    }
                    CurrentAction = ActionQueue.First();
                }
                else
                {
                    ++Index;
                    if (Index == ActionQueue.Count)
                    {
                        CurrentAction = ActionQueue[0];
                        Index = 0;
                    }
                    CurrentAction = ActionQueue[Index];
                }
                //Restart the action to ensure that the starting value is 
                //equivelent to the last action's ending value.
                CurrentAction.Restart();
            }

            CurrentAction.Update(dt);
        }

        /// <summary>
        ///   Are there no Actions in queue?
        /// </summary>
        public bool IsEmpty()
        {
            return ActionQueue.Count == 0;
        }

        /// <summary>
        ///   Add action to queue.
        ///   Current action becomes new Action.
        /// </summary>
        public void AddAction(ActionBase action)
        {
            Completed = false;
            ActionQueue.Add(action);
            CurrentAction = ActionQueue.First();
        }

        /// <summary>
        ///   Resets sequence to starting state.
        ///   Calls Restart on all Actions in queue.
        ///   If queue is empty, complete instantly.
        /// </summary>
        public override void Restart()
        {
            Completed = false;
            Index = 0;
            
            if (ActionQueue.Count > 0)
            {
              CurrentAction = ActionQueue.First();
              while (Index < ActionQueue.Count)
              {
                ActionQueue[Index].Restart();
                ++Index;
              }
            }
            else
            {
                Completed = true;
            }
            Index = 0;
        }

        /// <summary>
        ///   Clear action queue and restart
        /// </summary>
        public void Clear()
        {
            ActionQueue.Clear();
            Restart();
        }

        /// <summary>
        ///   Clears action queue
        /// </summary>
        ~ActionSequence()
        {
            Clear();
        }
    }
}
