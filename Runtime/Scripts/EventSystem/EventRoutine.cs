﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEventRoutineInfo
{
    /// <value>
    ///   Should <c>KillRoutine</c> get called when <c>OnEventFunc</c>
    ///   is called.
    /// </value>
    bool KeepAlive { get; set; }
    /// <value>
    ///   Object events are connected to.
    /// </value>
    GameObject ListenTarget { get; set; }
    Events EventToListenFor { get; set; }
    /// <value>
    ///   Callback at event, recieves <c>EventRoutine</c>.
    /// </value>
    Action<EventRoutine> Callback { get; set; }
    EventRoutine CreateRoutine();
}

public struct EventRoutineInfo : IEventRoutineInfo
{
    public bool CloneIfInUse { get; set; }
    public bool KeepAlive { get; set; }
    public GameObject ListenTarget { get; set; }
    public Events EventToListenFor { get; set; }
    public Action<EventRoutine> Callback { get; set; }

    public EventRoutineInfo(GameObject listenTarget, Events eventToListenFor, Action<EventRoutine> callback, bool keepAlive = false, bool cloneIfInUse = false)
    {
        CloneIfInUse = cloneIfInUse;
        ListenTarget = listenTarget;
        EventToListenFor = eventToListenFor;
        Callback = callback;
        KeepAlive = keepAlive;
    }

    public EventRoutine CreateRoutine()
    {
        return EventRoutine.Create(ListenTarget, EventToListenFor, Callback, KeepAlive, CloneIfInUse);
    }

    public EventRoutine CreateRoutine<T>(T storedData)
    {
        return EventRoutine.Create(ListenTarget, EventToListenFor, Callback, storedData, KeepAlive);
    }
}
public struct EventRoutineInfo<T> : IEventRoutineInfo
{
    public bool KeepAlive { get; set; }
    public GameObject ListenTarget { get; set; }
    public Events EventToListenFor { get; set; }
    public Action<EventRoutine> Callback { get; set; }
    public T StoredData { get; set; }

    public EventRoutineInfo(GameObject listenTarget, Events eventToListenFor, Action<EventRoutine> callback, T storedData, bool keepAlive = false)
    {
        ListenTarget = listenTarget;
        EventToListenFor = eventToListenFor;
        Callback = callback;
        KeepAlive = keepAlive;
        StoredData = storedData;
    }

    public EventRoutine CreateRoutine()
    {
        return EventRoutine.Create(ListenTarget, EventToListenFor, Callback, StoredData, KeepAlive);
    }
}

/// <summary>
///   Represent a u-eat event connection from a gameobject to a
///   callback. through this handel the connection can be killed and
///   data emited by the event can be accessed.
/// </summary>
/// <remark>
///   Listens for "CullEventRoutines" events on <c>GameSession</c>,
///   each EventRoutine is killed, aka. it stops listening to events
///   and <c>Alive</c> is set to <c>false</c>.
/// </remark>
public class EventRoutine
{
    /// <summary>
    ///   A list of all <c>EventRoutine</c>s added with
    ///   <c>AddRoutine</c>.
    /// </summary>
    static List<WeakReference> EventRoutines = new List<WeakReference>();
    /// <summary>
    ///   Static constructor, called at some point before
    ///   <c>EventRoutine</c> is first instanciated.
    /// </summary>
    static EventRoutine()
    {
        Game.GameSession.Connect("CullEventRoutines", OnCullEventRoutines);
    }

    /// <summary>
    ///   Kill all <c>EventRoutine</c>s in <c>EventRoutines</c>.
    /// </summary>
    static void OnCullEventRoutines(EventData data)
    {
        foreach(var routineHandle in EventRoutines)
        {
            if(routineHandle.IsAlive)
            {
                var routine = routineHandle.Target as EventRoutine;
                routine.KillRoutine();
            }
        }
    }

    /// <summary>
    ///   Add <c>EventRoutine</c>, setting <c>RoutineIndex</c>.
    /// </summary>
    static void AddRoutine(EventRoutine routine)
    {
        routine.RoutineIndex = EventRoutines.Count;
        EventRoutines.Add(new WeakReference(routine));
    }

    /// <summary>
    ///   Remove routine at index, replacing it with the
    ///   <c>EventRoutine</c> currently at the end, updating its <c>RoutineIndex</c>
    ///   if <c>IsAlive</c> is true.
    /// </summary>
    static void RemoveRoutine(int routineIndex)
    {
        if(EventRoutines.Count == 0) return;
        if(EventRoutines.Back().IsAlive)
        {
            var routine = EventRoutines.Back().Target as EventRoutine;
            routine.RoutineIndex = routineIndex;
        }
        EventRoutines.Swap(routineIndex, EventRoutines.Count - 1);
        EventRoutines.PopBack();
    }

    static public EventRoutine Create(GameObject listenTarget,
                                      Events eventToListenFor,
                                      Action<EventRoutine> callback,
                                      bool keepAlive = false,
                                      bool cloneIfInUse = false) =>
        new EventRoutine(listenTarget, eventToListenFor, callback, keepAlive, cloneIfInUse);

    static public EventRoutine Create(GameObject listenTarget,
                                      Events eventToListenFor,
                                      Action callback,
                                      bool keepAlive = false,
                                      bool cloneIfInUse = false) =>
        new EventRoutine(listenTarget, eventToListenFor, callback, keepAlive, cloneIfInUse);

    static public EventRoutine Create<T>(GameObject listenTarget,
                                         Events eventToListenFor,
                                         Action<EventRoutine> callback,
                                         T storedData,
                                         bool keepAlive = false,
                                         bool cloneIfInUse = false) =>
        EventRoutine<T>.Create(listenTarget, eventToListenFor, callback, storedData, keepAlive, cloneIfInUse);

    static public EventRoutine Create<T>(GameObject listenTarget,
                                         Events eventToListenFor,
                                         Action callback,
                                         T storedData,
                                         bool keepAlive = false,
                                         bool cloneIfInUse = false) =>
        EventRoutine<T>.Create<T>(listenTarget, eventToListenFor, callback, storedData, keepAlive, cloneIfInUse);

    int RoutineIndex;
    public bool KeepAlive { get; protected set; }
    public GameObject ListenTarget { get; protected set; }
    public Events EventToListenFor { get; protected set; }
    public event Action<EventRoutine> Callback;
    public EventData RecievedData { get; protected set; }
    public bool IsAlive { get; protected set; }

    /// <summary>
    ///   Create new <c>EventRoutine</c>, connecting
    ///   <c>OnEventFunc</c> to <paramref name="eventToListenFor"/> on
    ///   <paramref name="listenTarget"/> immediatly.
    /// </summary>
    protected EventRoutine(GameObject listenTarget,
                           Events eventToListenFor,
                           Action<EventRoutine> callback,
                           bool keepAlive = false,
                           bool cloneIfInUse = false)
    {
        AddRoutine(this);
        ListenTarget = listenTarget;
        EventToListenFor = eventToListenFor;
        Callback = callback;
        KeepAlive = keepAlive;
        ListenTarget.Connect(EventToListenFor, OnEventFunc, cloneIfInUse);
        IsAlive = true;
    }

    protected EventRoutine(GameObject listenTarget,
                           Events eventToListenFor,
                           Action callback, bool keepAlive = false, bool cloneIfInUse = false) :
        this(listenTarget, eventToListenFor, (EventRoutine data) => { callback?.Invoke(); }, keepAlive, cloneIfInUse)
    {}

    /// <summary>
    ///   Stored <c>RecievedData</c>, calls <c>Callback</c> with
    ///   <c>this</c> if <c>Callback</c> is not null, then kills
    ///   <c>EventRoutine</c> if <c>KeepAlive</c> is false.
    /// </summary>
    protected virtual void OnEventFunc(EventData data)
    {
        RecievedData = data;
        if((Callback.Target != null && !Callback.Target.Equals(null)) || Callback.Method.IsStatic)
            Callback(this);

        if(!KeepAlive)
            KillRoutine();
    }

    /// <summary>
    ///   Disconnect <c>EventRoutine</c> and set <c>IsAlive</c> to
    ///   false.
    /// </summary>
    public virtual void KillRoutine()
    {
        if (ListenTarget && IsAlive)
        {
            ListenTarget.Disconnect(EventToListenFor, OnEventFunc);
            IsAlive = false;
        }
    }

    public T GetRecievedData<T>() where T: EventData =>
        (T)RecievedData;

    ~EventRoutine()
    {
        RemoveRoutine(RoutineIndex);
    }
}

/// <summary>
///   Like <c>EventRoutine</c> but stores extra data to be accessed by
///   callback.
/// </summary>
public class EventRoutine<T1> : EventRoutine
{
    static public EventRoutine<T1> Create(GameObject listenTarget,
                                          Events eventToListenFor,
                                          Action<EventRoutine> callback,
                                          T1 storedData,
                                          bool keepAlive = false,
                                          bool cloneIfInUse = false) =>
        new EventRoutine<T1>(listenTarget, eventToListenFor, callback, storedData, keepAlive, cloneIfInUse);

    static public EventRoutine<T1> Create<T>(GameObject listenTarget,
                                             Events eventToListenFor,
                                             Action callback,
                                             T1 storedData,
                                             bool keepAlive = false,
                                             bool cloneIfInUse = false) =>
        new EventRoutine<T1>(listenTarget, eventToListenFor, callback, storedData, keepAlive, cloneIfInUse);

    public T1 StoredData { get; set; }

    protected EventRoutine(GameObject listenTarget, Events eventToListenFor, Action<EventRoutine> callback, T1 storedData, bool keepAlive = false, bool cloneIfInUse = false) 
        : base(listenTarget, eventToListenFor, callback, keepAlive)
    {
        StoredData = storedData;
    }

    protected EventRoutine(GameObject listenTarget, Events eventToListenFor, Action callback, T1 storedData, bool keepAlive = false, bool cloneIfInUse = false)
        : base(listenTarget, eventToListenFor, callback, keepAlive)
    {
        StoredData = storedData;
    }
}
