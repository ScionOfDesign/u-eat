using System;
#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;

namespace CustomInspector
{
    public enum InspectorSortMode
    {
        PropertiesBeforeFields,
        FieldsBeforeProperties
    }

    internal interface ReferenceType
    {
        object GetValue();
    }

    public class ReferenceType<T> : ReferenceType
    {
        public T Value;
        public ReferenceType(T val)
        {
            Value = val;
        }

        public object GetValue()
        {
            return Value;
        }

        static public implicit operator T(ReferenceType<T> rhs)
        {
            return rhs.Value;
        }
        static public implicit operator ReferenceType<T>(T rhs)
        {
            return new ReferenceType<T>(rhs);
        }
    }

    static public class CustomInspectorScripts
    {
        //A private Unity function that gets the drawer type for the given class type.
        public static Func<Type, Type> GetDrawerTypeForType;
        public static Action<bool> SetBoldDefaultFont;
        //Add a function with the type that is being drawn for as the key and the draw function as the value in order for that type to be drawn when exposing a property.
        public static Dictionary<Type, Func<Rect, object, GUIContent, ICustomAttributeProvider, object>> TypeDrawFunctions = new Dictionary<Type, Func<Rect, object, GUIContent, ICustomAttributeProvider, object>>();
        //public static Dictionary<Type, Func<Rect, ValueType, GUIContent, ICustomAttributeProvider, ValueType>> ValueTypeDrawFunctions = new Dictionary<Type, Func<Rect, ValueType, GUIContent, ICustomAttributeProvider, ValueType>>();
        static CustomInspectorScripts()
        {
            //Unity should really make this public...
            var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
            var type = assembly.GetType("UnityEditor.ScriptAttributeUtility");
            MethodInfo info = type.GetMethod("GetDrawerTypeForType", BindingFlags.NonPublic | BindingFlags.Static);
            GetDrawerTypeForType = (Func<Type,Type>)Delegate.CreateDelegate(typeof(Func<Type, Type>), info);

            type = assembly.GetType("UnityEditor.EditorGUIUtility");
            info = type.GetMethod("SetBoldDefaultFont", BindingFlags.NonPublic | BindingFlags.Static);
            SetBoldDefaultFont = (Action<bool>)Delegate.CreateDelegate(typeof(Action<bool>), info);

            var customPropertyDrawers = UEATExtensions.GetTypesWithAttribute(typeof(CustomPropertyDrawer));
            foreach(var drawer in customPropertyDrawers)
            {
                foreach(var method in drawer.GetMethods(BindingFlags.Public | BindingFlags.Static))
                {
                    var methodAttr = method.GetCustomAttributes<ExposeDrawMethod>();
                    if (methodAttr.Length != 0)
                    {
                        var returnType = method.ReturnType;
                        if(methodAttr[0].ActualType != null)
                        {
                            returnType = methodAttr[0].ActualType;
                        }
                        if (returnType == null)
                        {
                            throw new Exception("Method of with the 'ExposeDrawMethod' attribute must have a return type.");
                        }
                        var func = (Func<Rect, object, GUIContent, ICustomAttributeProvider, object>)Delegate.CreateDelegate(typeof(Func<Rect, object, GUIContent, ICustomAttributeProvider, object>), method);
                        TypeDrawFunctions.Add(returnType, func);
                    }
                }
            }
        }

        public static string[] GetSortingLayerNames()
        {
            Type internalEditorUtilityType = typeof(UnityEditorInternal.InternalEditorUtility);
            PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
            return (string[])sortingLayersProperty.GetValue(null, new object[0]);
        }
        // Get the unique sorting layer IDs -- tossed this in for good measure
        public static int[] GetSortingLayerUniqueIDs()
        {
            Type internalEditorUtilityType = typeof(UnityEditorInternal.InternalEditorUtility);
            PropertyInfo sortingLayerUniqueIDsProperty = internalEditorUtilityType.GetProperty("sortingLayerUniqueIDs", BindingFlags.Static | BindingFlags.NonPublic);
            return (int[])sortingLayerUniqueIDsProperty.GetValue(null, new object[0]);
        }

        public static InspectorSortMode SortMode = InspectorSortMode.PropertiesBeforeFields;
        
        //static readonly object[] EmptyObjectArray = new object[] { };
        //static readonly Type[] EmptyTypeArray = new Type[0] { };
        
        const float ScriptReferencePixelPadding = 1f;
        const string UnityScriptRefName = "m_Script";
        public static void DrawScriptReference(this Editor me)
        {
            var prop = me.serializedObject.FindProperty(UnityScriptRefName);
            DrawScriptReference(prop);
        }

        static void DrawScriptReference(SerializedProperty prop)
        {
            if(prop.objectReferenceValue != null)
            {
                GUI.enabled = false;
            }
            EditorGUILayout.PropertyField(prop);
            GUI.enabled = true;
        }

        public static void DrawDefaultInspector(this Editor me, bool drawScriptReference)
        {
            var prop = me.serializedObject.GetIterator();
            if(!prop.NextVisible(true)) return;
            if(drawScriptReference)
            {
                DrawScriptReference(prop);
            }
            while(prop.NextVisible(false))
            {
                EditorGUILayout.PropertyField(prop);
            }
        }
    } 
}
#endif
[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
//Allows the property to be visible and editable in the inspector.
public class ExposeDrawMethod : Attribute
{
    public Type ActualType;
    public ExposeDrawMethod(Type actualType = null)
    {
        ActualType = actualType;
    }
}
