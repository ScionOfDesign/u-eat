﻿/****************************************************************************/
/*!
\file   ObjectPool.cs
\author Joshua Biggs
\par    Email: Joshua.B@Digipen.edu
\par    Developed: Summer 2016
\brief

The class contains a generic stack-based object pool.

© 2016 Joshua Biggs CC Attribution
*/
/****************************************************************************/
using System;
using System.Collections.Generic;

namespace ActionSystem
{
    public class ObjectPool<T> where T : new()
    {
        Stack<T> Objects;

        public ObjectPool(int startingSize = 0)
        {
            Objects = new Stack<T>(startingSize);
        }

        public T GetObject()
        {
            if (Objects.Count > 0)
            {
                return Objects.Pop();
            }
            return new T();
        }

        public void PutObject(T item)
        {
            Objects.Push(item);
        }
    }
}
