﻿using UnityEngine;
using System.Reflection;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public struct Pair<T1, T2>
{
    public T1 First;
    public T2 Second;

    public Pair(T1 first, T2 second)
    {
        First = first;
        Second = second;
    }
}

[System.AttributeUsage(System.AttributeTargets.Class | System.AttributeTargets.Struct, AllowMultiple = false, Inherited = true)]
public class DefaultUnityInspectorAttribute : System.Attribute
{

}

[System.Serializable]
public class SerializableDictionary<T1, T2>
{
    public List<T1> Keys;
    public List<T2> Values;

    public SerializableDictionary(Dictionary<T1, T2> dict)
    {
        Keys = new List<T1>(dict.Count);
        Values = new List<T2>(dict.Count);
        foreach(var pair in dict)
        {
            Keys.Add(pair.Key);
            Values.Add(pair.Value);
        }
    }

    public Dictionary<T1, T2> ToDictionary()
    {
        var dict = new Dictionary<T1, T2>(Keys.Count);
        for(int i = 0; i < Keys.Count; ++i)
        {
            dict.Add(Keys[i], Values[i]);
        }
        return dict;
    }
}

[System.Serializable]
public class StringIntDict : SerializableDictionary<string, int>
{
    public StringIntDict(Dictionary<string, int> dict) : base(dict)
    {

    }
}
