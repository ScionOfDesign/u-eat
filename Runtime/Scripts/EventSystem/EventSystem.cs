﻿/****************************************************************************/
/*!
\file   EventSystem.cs
\author Joshua Biggs
\par    Email: Joshua.B@Digipen.edu
\par    Developed: Summer 2016
\brief

This file contains the static EventSystem class. The methods in this class
make interfacing with the EventHandler components easier.

© 2016 Joshua Biggs CC Attribution
*/
/****************************************************************************/
using System;
using UnityEngine;


public static class EventSystem
{
    //An empty EventData object to be used when no data needs to be passed.
    //Could also be set to be whatever the user wants for debugging purpouses.
    public static EventData DefaultEventData = new EventData();

    /// <param name="target">
    ///   the GameObject you are listening to
    /// </param>
    /// <param name="eventName">
    ///   the string name of the event you are listening to
    /// </param>
    /// <param name="func">
    ///   the function you are calling. This function must only have
    ///   one parameter of type 'EventData' and return void.
    /// </param>
    public static void EventConnect(GameObject target, string eventName, Action<EventData> func, bool cloneIfInUse = false)
    {
        var targetHandler = target.GetComponent<EventHandler>();
        if(!targetHandler)
        {
            targetHandler = target.AddComponent<EventHandler>();
            targetHandler.Hide();
        }
        targetHandler.EventConnect(eventName, func, cloneIfInUse);
    }

    /// <param name="target">
    ///   the object you are disconnecting from.
    /// </param>
    /// <param name="eventName">
    ///   the name of the event you are disconnecting from.
    /// </param>
    /// <param name="thisPointer">
    ///   the object which hosts the function the event calls.
    /// </param>
    public static void EventDisconnect(GameObject target, String eventName, object thisPointer = null)
    {
        var targetHandler = target.GetComponent<EventHandler>();
        if (!targetHandler)
        {
            return;
        }
        targetHandler.EventDisconnect(eventName, thisPointer);
    }

    /// <param name="target">
    ///   the object you are disconnecting from.
    /// </param>
    /// <param name="eventName">
    ///   the name of the event you are disconnecting from.
    /// </param>
    /// <param name="function">
    ///   the function itself
    /// </param>
    /// <remark>
    ///   You must provide the function itself if you are
    ///   disconnecting a static function. Or null to disconnect ALL
    ///   static functions.
    /// </remark>
    public static void EventDisconnect(GameObject target, string eventName, Action<EventData> function)
    {
        var targetHandler = target.GetComponent<EventHandler>();
        if (!targetHandler)
        {
            return;
        }
        targetHandler.EventDisconnect(eventName, function);
    }

    /// <param name="target">
    ///   the object you are dispatching to.
    /// </param>
    /// <param name="eventName">
    ///   the string name of the event you are dispatching.
    /// </param>
    /// <param name="eventData">
    ///   instance of a class that derives from 'EventData'. If this
    ///   is not specified a default EventData is used.
    /// </param>
    public static void EventSend(GameObject target, string eventName, EventData eventData = null)
    {
        var targetHandler = target.GetComponent<EventHandler>();
        if (!targetHandler)
        {
            return;
        }
        targetHandler.EventSend(eventName, eventData);
    }

    public static void DisconnectObject(GameObject target)
    {
        var targetHandler = target.GetComponent<EventHandler>();
        if (!targetHandler)
        {
            return;
        }
        targetHandler.DisconnectAll();
    }

    //Extension Methods to the GameObject class.

    /// <summary>
    ///   Extension to call <c>EventSystem.EventSend</c> with the
    ///   first argument set to <c>this</c>.
    /// </summary>
    public static void DispatchEvent(this GameObject target, string eventName, EventData eventData = null)
    {
        EventSend(target, eventName, eventData);
    }
    /// <summary>
    ///   Extension to call <c>EventSystem.EventConnect</c> with the
    ///   first argument set to <c>this</c>.
    /// </summary>
    public static void Connect(this GameObject target, string eventName, Action<EventData> func, bool cloneIfInUse = false)
    {
        EventConnect(target, eventName, func, cloneIfInUse);
    }
    //Warning, may cause memory leaks if not used properly.
    public static void Connect(this GameObject target, string eventName, Action func, bool cloneIfInUse = false)
    {
        EventConnect(target, eventName, (EventData data) => { func(); }, cloneIfInUse);
    }
    /// <summary>
    ///   Extension to call <c>EventSystem.EventDisconnect</c> with
    ///   the first argument set to <c>this</c>.
    /// </summary>
    public static void Disconnect(this GameObject target, string eventName, Action<EventData> func)
    {
        EventDisconnect(target, eventName, func);
    }
    /// <summary>
    ///   Extension to call <c>EventSystem.EventDisconnect</c> with
    ///   the first argument set to <c>this</c>.
    /// </summary>
    public static void Disconnect(this GameObject target, string eventName, object funcThisPointer)
    {
        EventDisconnect(target, eventName, funcThisPointer);
    }
}

[Serializable]
//The default class that all custom events must inherit from.
public class EventData
{
    
}



