﻿/****************************************************************************/
/*!
\file   EventHandler.cs
\author Joshua Biggs
\par    Email: Joshua.B@Digipen.edu
\par    Developed: Summer 2016
\brief

This file contains the EventHandler class. This class manages what events
this object is listening to and what functions should be called when it
recieves an event.

© 2016 Joshua Biggs CC Attribution
*/
/****************************************************************************/
//#define DEBUG_EVENTS
using UnityEngine;
using System.Collections.Generic;
using System;

public class EventHandler : MonoBehaviour
{
#if DEBUG_EVENTS
    const bool PrintAll = true;
    static string[] EventsToDebug = { "SettingsLoaded" };
    static string[] EventsToExclude = { Events.LogicUpdate, Events.LateUpdate, Events.EngineUpdate, Events.FixedUpdate };
    bool ShouldPrint(string eventName)
    {
        return (PrintAll || EventsToDebug.Contains(eventName)) && !EventsToExclude.Contains(eventName);
    }
#endif
    //Is this component visible in the inspector on every object that is listening to events?
    const bool IsVisibleInInspector = false;
    //This Dictionary stores all the different lists of functions this object is going to call when it recieves an event.
    Dictionary<string, EventListData> EventList = new Dictionary<string, EventListData>();
    //Used for determining if an event is being disconnected from during another event call.
    //Stack<string> EventStack = new Stack<string>();
    void Awake()
    {
        Hide();
    }

    public void Hide()
    {
        if (!IsVisibleInInspector)
        {
            hideFlags = HideFlags.HideInInspector;
        }
    }
    //If the EventList does not have that event as a key, add it, then push the function to the list of functions to be called.
    public void EventConnect(string eventName, Action<EventData> func, bool cloneIfInUse = false)
    {
        if(string.IsNullOrEmpty(eventName) || func == null) return;
        EventListData data;
        if (!EventList.TryGetValue(eventName, out data))
        {
            EventList.Add(eventName, new EventListData(new List<Action<EventData>>() { func }));
        }
        else
        {
            if(cloneIfInUse && data.IsBeingModified)
            {
                data = CloneList(data);
                EventList[eventName] = data;
            }
            data.Events.Add(func);
        }
#if DEBUG_EVENTS
        if(ShouldPrint(eventName))
        {
            PrintFunctionInfo(func, "Connecting to object <b>" + gameObject.name + "</b> for event <b>" + eventName + "</b>.\n");
        }
#endif
    }

    //Prints debug information about the function.
    void PrintFunctionInfo(Action<EventData> func, string baseMessage = "")
    {
        string message;
        if (!func.Method.IsStatic)
        {
            message = "This event calls function <b>" + func.Method.Name + "</b>";
            if (!func.Target.Equals(null))
            {
                Component compTarget = func.Target as Component;
                if (compTarget)
                {
                    var gameObj = compTarget.gameObject;
                    int compIndex = gameObj.GetComponents(compTarget.GetType()).IndexOf(compTarget);
                    message += " on component <b>" + compTarget.GetType().Name + '[' + compIndex + "]</b> on object <b>" + gameObj.name + "</b>.";
                }
                else
                {
                    UnityEngine.Object objTarget = func.Target as UnityEngine.Object;
                    if (objTarget)
                    {
                        message += " on Unity object <b>" + objTarget.name + "</b>.";
                    }
                    else
                    {
                        message += " on object <b>" + func.Target + "</b>.";
                    }
                }
            }
            else
            {
                message += " but the Unity object it is being called on has been destroyed.";
            }
        }
        else
        {
            message = "This event calls the static function <b>" + func.Method.Name + "</b> on the class <b>" + func.Method.DeclaringType.Name + "</b>";
        }
        Debug.Log(baseMessage + message);
    }

    //Removes ALL the functions with the given this pointer from the function list.
    public void EventDisconnect(string eventName, object thisPointer = null)
    {
        EventListData data;
        if (!EventList.TryGetValue(eventName, out data))
        {
            return;
        }
        
        var functionList = data.Events;
        for (int i = 0; i < functionList.Count; ++i)
        {
            if (functionList[i].Target.Equals(thisPointer))
            {
                functionList.RemoveAt(i);
                --i;
            }
        }
        //If there are no more functions to be called for that event, remove it from the list.
        if (functionList.Count == 0)
        {
            EventList.Remove(eventName);
        }
#if DEBUG_EVENTS
        if (ShouldPrint(eventName))
        {
            Debug.Log("Disconnecting from object <b>" + gameObject.name + "</b> for all events <b>" + eventName + "</b>.");
        }
#endif
    }
    /*Removes the first equivalent function from the function list.
      If a function is connected to be called twice, it must be disconnected twice.*/
    public void EventDisconnect(string eventName, Action<EventData> func)
    {
        if(string.IsNullOrEmpty(eventName)) return;
        EventListData data;
        if (!EventList.TryGetValue(eventName, out data))
        {
            return;
        }
        var functionList = data.Events;
        for (int i = 0; i < functionList.Count; ++i)
        {
            if (functionList[i] == func)
            {
                //We can't simply remove the function because it will skip over the next function which is being called.
                if(data.IsBeingModified)
                {
                    EventList[eventName] = CloneList(data);
                    functionList = EventList[eventName].Events;
                }

                functionList.RemoveAt(i);
                break;
            }
        }
        //If there are no more functions to be called for that event, remove it from the list.
        if (functionList.Count == 0)
        {
            EventList.Remove(eventName);
        }

#if DEBUG_EVENTS
        if (ShouldPrint(eventName))
        {
            PrintFunctionInfo(func, "Disconnecting from object <b>" + gameObject.name + "</b> for event <b>" + eventName + "</b>.\n");
        }
#endif
    }

    //Calls all the functions associated with the given event, and passes them empty EventData.
    public void EventSend(string eventName)
    {
        EventSend(eventName, EventSystem.DefaultEventData);
    }

    //Calls all the functions associated with the given event, and passes them the given EventData.
    public void EventSend(string eventName, EventData eventData)
    {
        EventListData data;
        if (!EventList.TryGetValue(eventName, out data))
        {
#if DEBUG_EVENTS
        if (ShouldPrint(eventName))
        {
             Debug.Log("Calling on object <b>" + gameObject.name + "</b> for event <b>" + eventName + "</b> but no functions were found.");
        }
#endif
        return;
        }
        //Lock the list so that it must be copied if an event is removed while  it is being modified.
        EventList[eventName] = Lock(data);
        var functionList = data.Events;
        for (var i = 0; i < functionList.Count; ++i)
        {
            var func = functionList[i];
#if DEBUG_EVENTS
            if (EventsToDebug.Contains(eventName))
            {
                PrintFunctionInfo(func, "Calling on object <b>" + gameObject.name + "</b> for event <b>" + eventName + "</b> the following function:\n");
            }
#endif
            //I call the virtual Equals function to specifically check if the UnityEngine.Object was nulled.
            //This only works thanks to UnityEngine.Object nulls all references to itself when it is destroyed.
            if (func.Method.IsStatic || !func.Target.Equals(null))
            {
                func(eventData);
            }
            else
            {
                //Remove any invalid functions.
                functionList[i] = functionList[functionList.Count - 1];
                functionList.RemoveAt(functionList.Count - 1);
                --i;
            }
        }
        //The key may have been removed in one of the called functions.
        if (EventList.TryGetValue(eventName, out data))
        {
            EventList[eventName] = Unlock(data);
        }
    }

    //Clears the Dictionary of all events.
    public void DisconnectAll()
    {
        EventList.Clear();
    }

    //The list in this struct is only cloned if an event is removed from its list WHILE it is being modified.
    struct EventListData
    {
        public bool IsBeingModified;
        public List<Action<EventData>> Events;

        public EventListData(List<Action<EventData>> list, bool isBeingModified = false)
        {
            IsBeingModified = isBeingModified;
            Events = list;
        }
    }
    EventListData Lock(EventListData data)
    {
        data.IsBeingModified = true;
        return data;
    }

    EventListData Unlock(EventListData data)
    {
        data.IsBeingModified = false;
        return data;
    }
    EventListData CloneList(EventListData data)
    {
        //It has been cloned. It is no longer locked.
        var newData = new EventListData(new List<Action<EventData>>(data.Events), false);
        return newData;
    }
}
