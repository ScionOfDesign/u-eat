using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor.Callbacks;
using UnityEditor;
[InitializeOnLoad]
#endif

/// <remark>
///  Event which go throug GameSession
///  
///  <list type="bullet">
///   <item>
///    <term>"UIOff"</term>
///    <description>
///     hide toggleable ui
///    </description>
///   </item>
///   <item>
///    <term>"UIOn"</term>
///    <description>
///     show toggleable ui
///    </description>
///   </item>
///   <item>
///    <term>"UIInitialized"</term>
///    <description>
///     set UIInitialized to true
///    </description>
///   </item>
///   <item>
///    <term>"GameLoaded"</term>
///    <description>
///     - calls ~MouseMinicon.OnGameLoaded~
///    </description>
///   </item>
///   <item>
///    <term>"SetMusic"</term>
///    <description>
///     Recieves SetMusicEvent.
///     - Sets clip of secondary audio source.
///     - fade out primary audio source.
///     - fade in secondary audio source.
///     - make primary source secondary source and vice versa
///    </description>
///   </item>
///   <item>
///    <term>"SetMusicVolume"</term>
///    <description>
///     Recieves ~SetMusicVolumeEvent data~ Fades volume of primary audio
///     source to ~data.Volume~ over the time ~data.Duration~ seconds.
///    </description>
///   </item>
///   <item>
///    <term>"EscapePressed"</term>
///    <description>
///     Triggered when escape pressed.
///    </description>
///   </item>
///   <item>
///    <term>"ResetState"</term>
///    <description>
///     - calls ~MouseMinicon.OnGameLoaded~
///    </description>
///   </item>
///   <item>
///    <term>"SetMinicon"</term>
///    <description>
///     - calls ~MouseMinicon.OnSetMinicon~
///     - dispatched by InventoryManager.SelectedObject setter
///    </description>
///   </item>
///   <item>
///    <term>Events.Create</term>
///    <description>
///     Dispatched by GameEventHandler.Awake
///    </description>
///   </item>
///   <item>
///    <term>Events.Initialize</term>
///    <description>
///     Dispatched by GameEventHandler.Start
///    </description>
///   </item>
///   <item>
///    <term>Events.LogicUpdate</term>
///    <description>
///     Dispatched by GameEventHandler.Update
///    </description>
///   </item>
///   <item>
///    <term>Events.LateUpdate</term>
///    <description>
///     Dispatched by GameEventHandler.LateUpdate
///    </description>
///   </item>
///   <item>
///    <term>Events.FixedUpdate</term>
///    <description>
///     Dispatched by GameEventHandler.FixedUpdate
///    </description>
///   </item>
///   <item>
///    <term>Events.ApplicationGainFocus</term>
///    <description>
///     Dispatched by GameEventHandler.OnApplicationFocus when called with true
///    </description>
///   </item>
///   <item>
///    <term>Events.ApplicationLoseFocus</term>
///    <description>
///     Dispatched by GameEventHandler.OnApplicationFocus when called with false
///    </description>
///   </item>
///   <item>
///    <term>Events.ApplicationPause</term>
///    <description>
///     Dispatched by GameEventHandler.OnApplicationPause when called with true
///    </description>
///   </item>
///   <item>
///    <term>Events.ApplicationResume</term>
///    <description>
///     Dispatched by GameEventHandler.OnApplicationPause when called with true
///    </description>
///   </item>
///   <item>
///    <term>Events.Destroy</term>
///    <description>
///     Dispatched by GameEventHandler.OnApplicationQuit
///    </description>
///   </item>
///   <item>
///    <term>"ResetSettings"</term>
///    <description>
///     Calls Settings.OnResetAllSettings. Called by clicking the "Reset to
///     Defaults" button in the options menu.
///    </description>
///   </item>
///   <item>
///    <term>"ResetVariables"</term>
///    <description>
///     Calls Settings.OnResetAllCharacters
///    </description>
///   </item>
///   <item>
///    <term>"CharacterSpeaking"</term>
///    <description>
///     Dispatched by DialogueManager.
///     Recieves StringEvent(CharacterSettings.FullName)
///    </description>
///   </item>
///   <item>
///    <term>"CharacterSpeakInfo"</term>
///    <description>
///     Dispatched by DialogueManager.
///     Recieves EventData<CharacterSettings>
///    </description>
///   </item>
///   <item>
///    <term>"LoadSettings"</term>
///    <description>
///     Calls SaveGameManager.LoadSettings
///    </description>
///   </item>
///   <item>
///    <term>"SettingsSaved"</term>
///    <description>
///     Called when settings saved to disk.
///    </description>
///   </item>
///   <item>
///    <term>"OpenOptionsMenu"</term>
///    <description>
///     When Canvas recieves this event, activates OptionsMenu.
///    </description>
///   </item>
///  </list>
/// </remark>
public class Game : MonoBehaviour
{
    public bool EngineUpdate = true;
    static public float GameTimeScale
    {
        get
        {
            return Time.timeScale;
        }
        set
        {
            //Time.fixedDeltaTime = value * 0.02f;
            Time.timeScale = value;
            GameSession.DispatchEvent("TimeScaleChanged");
        }
    }
    //[ExposeProperty]
    public float TimeScale
    {
        get
        {
            return _TimeScale;
        }
        set
        {
            _TimeScale = value;
        }
    }
    static public bool Paused
    {
        get
        {
            return PausedProp;
        }
        set
        {
            if(value != PausedProp)
            {
                PausedProp = value;
                GameSession.DispatchEvent("PausedStateChanged");
            }
        }
    }
    static public bool GameWasPaused { get; private set; }
    static public bool GameWasPlaying { get; private set; }

    static bool PausedProp = false;
    [FormerlySerializedAs("TimeScaleProp")]
    static float _TimeScale = 1;
    Game()
    {
#if UNITY_EDITOR
        EditorApplication.update += GlobalUpdate;
#endif
    }

    private static GameObject _GameSession;
    public static GameObject GameSession
    {
        get
        {
            if(!_GameSession)
            {
                _GameSession = Resources.Load<GameObject>("Prefabs/GameSession");
                if(!_GameSession)
                {
                    Debug.Log("It is recommended that you create a 'GameSession' prefab in the resources folder.");
                    _GameSession = new GameObject("GameSession", typeof(Game));
                }
            }
            return _GameSession;
        }
    }
    static Game GameComp { get; set; }
    static GameEventHandler GameEventHandler { get; set; }

    static GameObject HandlerResource;

    static bool HaveInitialized = false;
    static ObjectActions Actions;
    static Game()
    {
    }
#if UNITY_EDITOR
    [InitializeOnLoadMethod]
#endif
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void InitializeGame()
    {
        
        GameComp = GameSession.GetOrAddComponent<Game>();
        //GameSession.Connect(Events.Create, GameComp.OnGameCreate);
        //GameSession.Connect(Events.Initialize, GameComp.OnGameInitialize);
        GameSession.Connect(Events.Update, GameComp.OnLogicUpdate);
        Actions = GameComp.GetOrAddComponent<ObjectActions>();
        GameSession.Connect(Events.LateUpdate, GameComp.OnGameLateUpdate);
        //GameSession.Connect(Events.Destroy, GameComp.OnGameDestroy);

        HandlerResource = Resources.Load<GameObject>("Prefabs/GameEventHandler");
        if (!HandlerResource)
        {
            HandlerResource = new GameObject("GameEventHandler", typeof(GameEventHandler), typeof(DontDestroyOnLoad));
        }
    }

    static public Coroutine StartGlobalCoroutine(IEnumerator routine)
    {
        return GameEventHandler.StartCoroutine(routine);
    }

    private void OnGameLateUpdate(EventData obj)
    {
        //Debug.Log("LATE UPDATE");
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    public static void InitializeGameEventHandler()
    {
        if(!GameEventHandler)
        {
            GameEventHandler = Instantiate(HandlerResource).GetComponent<GameEventHandler>();
        }
    }

    //void OnGameCreate(EventData data)
    //{
    //    Debug.Log("Game Started");
    //}

    //void OnGameInitialize(EventData data)
    //{
    //    Debug.Log("Game Initialized");
    //}

    void GlobalUpdate()
    {
        GameWasPlaying = Application.isPlaying;
        if(GameSession == null)
        {
            if (!HaveInitialized)
            {
                Debug.Log("GameSession is null. Attempting to load again in case this is the first ever time running U-EAT");
                HaveInitialized = true;
                InitializeGame();

                if(GameSession != null)
                {
                    Debug.Log("Success! You can now clear the console and this error should no longer occur.");
                }
                else
                {
                    Debug.Log("Error! Please double-check that the U-EAT GameSession Prefab is correctly configured.");
                }
            }
            else
            {
                return;
            }
        }
        if(EngineUpdate && !Application.isPlaying)
        {
            GameSession.DispatchEvent(Events.EngineUpdate);
        }
    }

    void OnLogicUpdate(EventData data)
    {
        GameWasPaused = Paused;
        Actions.Update();
    }


    //~Game()
    //{
    //    //GameSession.Disconnect(Events.LateUpdate, GameComp.OnGameLateUpdate);
    //}
    //void OnGameDestroy(EventData data)
    //{
    //    Debug.Log("Game Quitting");
    //}
}
