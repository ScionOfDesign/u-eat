﻿/****************************************************************************/
/*!
    \author Joshua Biggs  
    © 2015 DigiPen, All Rights Reserved.
*/
/****************************************************************************/
using ActionSystem;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
public static class UEATExtensions
{
    #region EventExtensions
    public static ActionSequence DispatchEventNextTwoFrames(this GameObject instance, string eventName, EventData data = null)
    {
        var seq = ActionSystem.Action.Sequence(instance.GetActions());
        ActionSystem.Action.Delay(seq, Time.deltaTime / 2);
        ActionSystem.Action.Call(seq, EventSystem.DispatchEvent, instance, eventName, data);
        return seq;
    }

    /// <summary>
    ///   Create <c>EventRoutine</c> which Listens on
    ///   <c>GameSession</c> for <c>Events.LogicUpdate</c> then it
    ///   dispatches event <paramref name="eventName"/> to <paramref
    ///   name="instance"/> with <paramref name="data"/>. It then
    ///   kills the <c>EventRoutine</c>.
    /// </summary>
    public static EventRoutine DispatchEventNextFrame(this GameObject instance, string eventName, EventData data = null)
    {
        return EventRoutine.Create(Game.GameSession, Events.Update, (EventRoutine routine) => {
            instance.DispatchEvent(eventName, data);
        }, false, false);
    }

    public static ActionSequence DispatchDelayedEvent(this GameObject instance, string eventName, float delay, EventData data = null)
    {
        var seq = ActionSystem.Action.Sequence(instance.GetActions());
        ActionSystem.Action.Delay(seq, delay);
        ActionSystem.Action.Call(seq, EventSystem.DispatchEvent, instance, eventName, data);
        return seq;
    }
    #endregion
    #region ActionExtensions
    public static EventRoutine CallFunctionNextFrame(this GameObject instance, System.Action func)
    {
        return EventRoutine.Create(Game.GameSession, Events.Update, (EventRoutine routine) => {
            func();
        }, false, false);
    }

    public static EventRoutine CallFunctionOnLateUpdate(this GameObject instance, System.Action func)
    {
        return EventRoutine.Create(Game.GameSession, Events.LateUpdate, (EventRoutine routine) => {
            func();
        }, false, false);
    }

    public static EventRoutine CallFunctionNextFrame<T>(this GameObject instance, System.Action<T> func, T arg1)
    {
        return EventRoutine.Create(Game.GameSession, Events.Update, (EventRoutine routine) => {
            func(arg1);
        }, false, false);
    }

    public static ActionSequence CallDelayedFunction(this GameObject instance, System.Action func, float delay)
    {
        var seq = ActionSystem.Action.Sequence(instance.GetActions());
        ActionSystem.Action.Delay(seq, delay);
        ActionSystem.Action.Call(seq, func);
        return seq;
    }

    public static ActionSequence CallDelayedFunction<T>(this GameObject instance, System.Action<T> func, T arg1, float delay)
    {
        var seq = ActionSystem.Action.Sequence(instance.GetActions());
        ActionSystem.Action.Delay(seq, delay);
        ActionSystem.Action.Call(seq, func, arg1);
        return seq;
    }
    #endregion
    #region ReflectionExtensions
    public static T[] GetCustomAttributes<T>(this ICustomAttributeProvider me, bool includeInheritedTypes = false) where T : Attribute
    {
        return (T[])me.GetCustomAttributes(typeof(T), includeInheritedTypes);
    }


    public static bool IsDerivedFrom(this Type me, Type otherType, bool includeSelf = true)
    {
        if(!includeSelf && otherType == me)
        {
            return false;
        }
        return otherType.IsAssignableFrom(me);
    }

    static public IEnumerable<Type> GetDerivedTypes(this Assembly assembly, Type baseType, bool includeSelf = false)
    {
        foreach(Type type in assembly.GetTypes())
        {
            if(type.IsDerivedFrom(baseType, includeSelf))
            {
                yield return type;
            }
        }
    }

    static public IEnumerable<Type> GetDerivedTypes(this Type baseType, bool includeSelf = false)
    {
        foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach(Type type in assembly.GetTypes())
            {
                if(type.IsDerivedFrom(baseType, includeSelf))
                {
                    yield return type;
                }
            }
        }
    }

    static public bool HasAttribute(this ICustomAttributeProvider me, Type attributeType, out Attribute[] attributes)
    {
        attributes = me.GetCustomAttributes(attributeType, true) as Attribute[];
        return attributes.Length > 0;
    }

    static public bool HasAttribute(this ICustomAttributeProvider me, Type attributeType)
    {
        var attributes = me.GetCustomAttributes(attributeType, true) as Attribute[];
        return attributes.Length > 0;
    }

    static public IEnumerable<Type> GetTypesWithAttribute(this Assembly assembly, Type attributeType)
    {
        foreach(Type type in assembly.GetTypes())
        {
            if(type.GetCustomAttributes(attributeType, true).Length > 0)
            {
                yield return type;
            }
        }
    }

    static public IEnumerable<Type> GetTypesWithAttribute(Type attributeType)
    {
        foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
        {
            foreach(Type type in assembly.GetTypes())
            {
                if(type.GetCustomAttributes(attributeType, true).Length > 0)
                {
                    yield return type;
                }
            }
        }
    }
    #endregion
    #region ArrayExtensions
    public static bool Contains<T>(this T[] instance, T param)
    {
        for(int i = 0; i < instance.Length; ++i)
        {
            if(instance[i].Equals(param))
            {
                return true;
            }
        }

        return false;
    }
    
    public static T Front<T>(this T[] instance)
    {
        return instance[0];
    }
    public static T Front<T>(this IList<T> instance)
    {
        return instance[0];
    }
    public static T Back<T>(this T[] instance)
    {
        return instance[instance.Length - 1];
    }
    public static T Back<T>(this IList<T> instance)
    {
        return instance[instance.Count - 1];
    }

    /// <summary>
    ///   Remove and return last instance in list.
    /// </summary>
    public static T PopBack<T>(this IList<T> instance)
    {
        var back = instance.Back();
        instance.RemoveAt(instance.Count - 1);
        return back;
    }

    public static T RandomFrom<T>(this IList<T> instance)
    {
        if(instance.Count > 0)
        {
            return instance[UnityEngine.Random.Range(0, instance.Count)];
        }
        return default(T);
    }
    public static T RandomFrom<T>(this T[] instance)
    {
        if(instance.Length > 0)
        {
            return instance[UnityEngine.Random.Range(0, instance.Length)];
        }
        return default(T);
    }

    /// <summary>
    ///   Swap positions of the instance in the list at <paramref
    ///   name="indexA"/> and that at <paramref name="indexB"/>.
    /// </summary>
    public static void Swap<T>(this IList<T> instance, int indexA, int indexB)
    {
        var temp = instance[indexA];
        instance[indexA] = instance[indexB];
        instance[indexB] = temp;
    }

    public static int IndexOf<T>(this T[] instance, T param)
    {
        for(int i = 0; i < instance.Length; ++i)
        {
            if(EqualityComparer<T>.Default.Equals(instance[i], param))
            {
                return i;
            }
        }

        return -1;
    }
    #endregion
}